const express = require('express');
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(cors());
const multer = require('multer');
const upload = multer();
// API 
const router = express.Router();
const TaskController = require('./controllers/TaskController.ts');
// Task
router.get('/tasks', TaskController.getTasks);
router.post('/tasks', TaskController.createTask);
router.get('/tasks/:id', TaskController.getTaskById);
router.put('/tasks/',upload.none(), TaskController.updateTaskById);
router.delete('/tasks/:id', TaskController.deleteTaskById);

app.use('/api', router); 

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
    console.log(`http://localhost:${PORT}`);
});
