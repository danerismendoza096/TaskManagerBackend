const knex = require('knex');
const knexConfig = require('../knexfile.js');
const db = knex(knexConfig.development);

exports.getTasks = async (req, res) => {
  try {
    const tasks = await db('tasks').select('tasks.*')
    res.json(tasks);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error', error: error });
  }
};

exports.createTask = async (req, res) => {
  try {
    const { title, status } = req.body;
    if (!title || !status) {
      return res.status(400).json({ message: 'Title and status are required' });
    }
    const [taskId] = await db('tasks').insert({ title, status });
    if (taskId) {
      res.status(200).json({ message: 'Task created successfully!'});
    } else {
      res.status(500).json({ message: 'Failed to create task' });
    }
  } catch (error) {
    console.error('Error inserting into database:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

exports.getTaskById = async (req, res) => {
  try {
    const { id } = req.params;
    const task = await db('tasks').where({ id }).first();
    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }
    res.json(task);
  } catch (error) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

exports.updateTaskById = async (req, res) => {
  try {
    const { id, status } = req.body;
    if (!status) {
      return res.status(400).json({ message: 'Status is required' });
    }
    const updatedCount = await db('tasks').where({ id }).update({ status });
    if (updatedCount === 0) {
      return res.status(404).json({ message: 'Task not found' });
    }
    res.status(200).json({ message: 'Task updated successfully'});
  } catch (error) {
    console.error('Error updating task:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

exports.deleteTaskById = async (req, res) => {
  try {
    const { id } = req.params;
    const deletedCount = await db('tasks').where({ id }).del();
    if (deletedCount === 0) {
      return res.status(404).json({ message: 'Task not found' });
    }
    res.status(200).json({ message: 'Task deleted successfully!' });
  } catch (error) {
    console.error('Error deleting task:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};