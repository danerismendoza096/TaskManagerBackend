module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: '127.0.0.1',
      user: 'root',
      password: '',
      database: 'task_manager_db',
    },
    migrations: {
      directory: __dirname + '/db/migrations',
    },
  },
};
